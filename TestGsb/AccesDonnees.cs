﻿using System;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TestGsb
{
    /// <summary>
    /// Classe d'accès aux données
    /// </summary>
    public class AccesDonnees
    {
        /// <summary>
        /// propriétés de la classe
       /// </summary>
        private MySqlConnection connection; // chaine de connexion
        private MySqlCommand command; // envoi de la requête à la base de données
        private MySqlDataReader reader; // gestion du curseur

        /// <summary>
        /// le Constructeur
        /// </summary>
        public AccesDonnees()
        {
            this.InitConnexion();
        }

        /// <summary>
        /// Méthode qui initialise la connexion
        /// </summary>
        /// <returns>Vrai si la connexion se fait; faux sinon</returns>
        public bool InitConnexion()
        {
            String chaineConnexion = "server=localhost; database=gsb_frais; user id=root; pwd="; // on envoie ces 4 paramètres
            this.connection = new MySqlConnection(chaineConnexion); // Création de l'objet connection
            try
            {
                connection.Open();
                // Console.WriteLine("Connexion à la base"); 
                return true;
            }
            catch (Exception ex)
            {
                // Console.WriteLine("Erreur de connexion");
                // System.Environment.Exit(1); // on quitte l'application si la connexion est impossible
                return false;

            }
        }

        /// <summary>
        /// va gérer les req d'affichage type select (lecture)
        /// </summary>
        /// <param name="requete">Requête à exécuter</param>
        public String requeteLecture(string requete)
        {
            this.command = new MySqlCommand(requete, this.connection);
            this.reader = this.command.ExecuteReader();
            StringBuilder sb = new StringBuilder(); // va gérer l'affichage dans la console si on veut contrôler l'output
            if (reader.HasRows)
            {
                if (sb.Length > 0) sb.Append("___");
                while (reader.Read()) // tant que la fin du curseur n'est pas atteinte 
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                        if (reader.GetValue(i) != DBNull.Value)
                            sb.AppendFormat("{0}-", Convert.ToString(reader.GetValue(i)));
                }
            }
            // Console.WriteLine(sb); // on peut afficher les données mais c'est optionnel 
            this.command.Dispose(); // ferme le curseur
            return sb.ToString();
        }

        /// <summary>
        /// Va gérer les req d'administration type update (modification)
        /// </summary>
        /// <param name="requete">Requête à exécuter</param>
        public void requeteAdmin(string requete)
        {
            this.command = new MySqlCommand(requete, this.connection);
            this.command.ExecuteNonQuery(); // exécute la requete
            this.command.Dispose(); // ferme le curseur
        }

        /// <summary>
        /// Ferme la connexion
        /// </summary>
        public void close()
        {
            this.connection.Close();
            // Console.WriteLine("Connexion terminée");
        }
    }
}
