﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGsb
{
    /// <summary>
    /// Classe pour la gestion des dates
    /// </summary>
    public abstract class GereDates
    {
        /// <summary>
        /// Méthode pour obtenir le mois précédent
        /// </summary>
        /// <returns>Le mois précédent de la date du jour</returns>
        public static String getMoisPrecedent()
        {
            return getMoisPrecedent(DateTime.Today);
        }

        /// <summary>
        /// Surcharge de la méthode pour obtenir le mois précédent
        /// </summary>
        /// <param name="date">Une date différente de la date du jour</param>
        /// <returns>Le mois précédent pour la date passée en paramètre</returns>
        public static String getMoisPrecedent(DateTime date)
        {
            DateTime moisDate = date.AddMonths(-1);
            String moisPrecedent = moisDate.ToString("MM"); // extrait le mois avec un zéro devant
            return moisPrecedent;
        }

        /// <summary>
        /// Méthode pour obtenir le mois suivant
        /// </summary>
        /// <returns>Le mois suivant pour la date du jour</returns>
        public static String getMoisSuivant()
        {
            return getMoisSuivant(DateTime.Today);
        }

        /// <summary>
        /// Surcharge de la méthode pour obtenir le mois suivant 
        /// </summary>
        /// <param name="date">Date différente de la date du jour</param>
        /// <returns>Le mois suivant à une date différente de la date du jour</returns>
        public static String getMoisSuivant(DateTime date)
        {
            DateTime moisDate = date.AddMonths(1);
            String moisSuivant = moisDate.ToString("MM"); // extrait le mois avec un zéro devant
            return moisSuivant;
        }

        /// <summary>
        /// Teste si la date du jour se situe entre deux bornes
        /// </summary>
        /// <param name="jour1">Borne inférieure à tester</param>
        /// <param name="jour2">Borne supérieure à tester</param>
        /// <returns>Vrai si entre, faux sinon</returns>
        public static Boolean entre(int jour1, int jour2)
        {
            return entre(jour1, jour2, DateTime.Today);
        }

        /// <summary>
        /// Surcharge de la méthode précédente pour savoir si le jour se situe entre deux bornes
        /// </summary>
        /// <param name="jour1">Borne inférieure à tester</param>
        /// <param name="jour2">Borne supérieure à tester</param>
        /// <param name="date">Une date différente de la date du jour</param>
        /// <returns>Vrai si entre, faux sinon</returns>
        public static Boolean entre(int jour1, int jour2, DateTime date)
        {
            int jourComparaison = date.Day; // on extrait le jour à partir de la date
            if (jourComparaison >= jour1 && jourComparaison <= jour2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
