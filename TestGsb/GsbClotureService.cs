﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TestGsb
{
    /// <summary>
    /// Classe d'exécution du service Windows
    /// </summary>
    public partial class GsbClotureService : ServiceBase
    {
        private int eventId = 1;

        public GsbClotureService()
        {
            InitializeComponent();

            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog"; 
        }

        /// <summary>
        /// Code exécuté au démarrage du service Windows
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart");
            // Set up a timer that triggers every interval
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 600000; // S'exécute toutes les heures et demi environ
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        /// <summary>
        /// Code exécuté à chaque intervalle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);

            // on établit un accès à la base
            AccesDonnees acces = new AccesDonnees();

            // quel est le mois précédent ?
            String moisPrecedent = GereDates.getMoisPrecedent();
            // de quelle année ? 
            String annee = DateTime.Today.ToString("yyyy");
            // on assemble les deux pour pouvoir identifier les fiches dans la base 
            String moisId = annee + moisPrecedent;
            //test sur le jour "entre"
            // on va vérifier qu'on est entre le 1 et le 10
            if (GereDates.entre(1, 10))
            {
                String sb = acces.requeteLecture("select * from fichefrais where mois =" + moisId + " and idetat='CR'"); // affichage des fiches à modifier
                eventLog1.WriteEntry(sb, EventLogEntryType.Information);
                // on met à jour les fiches
                acces.requeteAdmin("update fichefrais set idetat='CL' where mois =" + moisId + " and idetat='CR'");
            }
            else if (GereDates.entre(20, 31))
            {
                String sb = acces.requeteLecture("select * from fichefrais where mois =" + moisId + " and idetat='MP'");
                eventLog1.WriteEntry(sb, EventLogEntryType.Information);
                acces.requeteAdmin("update fichefrais set idetat='RB' where mois =" + moisId + " and idetat='MP'");
            }

            // on ferme l'accès à la base 
            acces.close();
        }

        /// <summary>
        /// Code effectué à l'arrêt du service windows
        /// </summary>

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In OnStop.");
        }
    }
}
