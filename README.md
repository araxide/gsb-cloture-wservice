# Windows Service Clôture des frais

** _Note sur l'avancement du projet_ **

Le codage de ce Windows Service est terminé.

## ** Technologies utilisées **

Base de données SQL

Visual Studio Enterprise 2017

C# utilisant le framework .NET Core 2.0 


## ** But de l'application **

Ce projet a été codé dans le cadre du contexte GSB pour le BTS SIO (session 2019). 

Il s'agit de pouvoir se connecter à une base de données MySQL pour effectuer des vérifications et des modifications selon la date du jour. La base de données recense des fiches de frais mensuelles et doit modifier périodiquement leur état. Ainsi : 

* à partir du 1er jour du mois N, et jusqu'au 10e jour, toutes les fiches non clôturées (CR) crées au mois N-1 passent en statut CL (clôturé) *
 
* à partir du 20e jour du mois N, les fiches crées au mois N-1 et mises en paiement (MP) passent en statut RB (remboursement) * 

Le projet se présente sous forme d'un Windows Service utilisant une classe de gestion des dates et une autre classe d'accès aux données. Un installeur a été prévu pour ce Windows Service. Le script se lance ainsi en tâche de fond et met à jour automatiquement la base de données. 


## ** Installer le Windows Service **

Il faut naviguer dans le dossier GsbSetupService ==> Release et double cliquer sur l'exécutable pour lancer l'installation.

Une fois l'installation terminée, vous devrez démarrer le service GsbClotureService à partir de la liste des services Windows (accessible via : touche windows + R / services.msc ).

Vous retrouverez des informations sur l'exécution de ce service dans l'Observateur d'évènements Windows.


## ** Note importante sur l'utilisation **

Pour tester l'interaction avec la base de données, vous devrez implémenter la base sur votre propre serveur à l'aide du script SQL nommé gsb_frais, et modifier les paramètres de connexion dans la méthode InitConnexion() de la classe AccesDonnees.cs. 


